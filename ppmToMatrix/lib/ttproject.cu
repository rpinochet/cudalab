#ifndef TTPROJECT_CU
#define TTPROJECT_CU

#include "ttproject.cuh"

using namespace std;

Imagen::Imagen() {
	
	f     = 0;
	c     = 0;
	color = 0;
	pixel = NULL;
}

Imagen::Imagen(int alto, int ancho, int maximo) {
	
	f     = alto;
	c     = ancho;
	color = maximo;
	pixel = new int**[f];
	
	for(int i = 0; i < f; i++) {
		
		pixel[i] = new int*[c];
		
		for(int j = 0; j < c; j++) {
			
			pixel[i][j]    = new int[3];
			pixel[i][j][0] = 0;
			pixel[i][j][1] = 0;
			pixel[i][j][2] = 0;
		}
	}
}	

Imagen::~Imagen() {
	
}

void Imagen::getMapa(int ***m) {
	
	pixel = m;
}
 
void Imagen::mostrarMapa() {
		
	for(int x = 0; x < f; x++) {
		for(int y = 0; y < c; y++) {
			cout << pixel[x][y][0] << "," << pixel[x][y][1] << "," << pixel[x][y][2] << "    ";
		}
		cout<<endl;
	}
}

/*
 * 
 * DECLARACION DE FUNCIONES =====================================================================
 * 
 * 
*/

/**
 * @brief De un string alto espacio ancho, solo retorna el ancho en entero.
 * @param t String formato alto espacio ancho ej "500 500"
 * @return Retorna el ancho como integer
 ** */
int ancho(char *t) {
	
	int c       = 0;
	int valor   = -1;
	char aux[20]; aux[0] = '\0';
	
	
	for(int i = 0; i < strlen(t); i++) {
		
		if(t[i] != ' ') {
			c        = strlen(aux);
			aux[c]   = t[i];
			aux[c+1] = '\0';
		}
		else{
			break;
		}
	}
	valor = atoi(aux);
	
	if(valor != -1)
		return valor;
	else
		return -1;
}

/**
 * @brief De un string alto espacio ancho, solo retorna el alto en entero.
 * @param t String formato alto espacio ancho ej "500 500"
 * @return Retorna el alto como integer
 ** */
int alto(char *t) {
	
	int c = 0;
	char aux[20]; aux[0] = '\0';
	int valor = -1;
	bool leer = false;
	
	for(int i=0; i<strlen(t); i++) {
		
		if(t[i] == ' ') {
			leer = true;
			continue;
		}
		else {
			if(leer) {
				c        = strlen(aux);
				aux[c]   = t[i];
				aux[c+1] = '\0';
			}
		}
	}
	valor = atoi(aux);
	if(valor != -1)
		return valor;
	else
		return -1;
		
}

/**
 * @brief Lee de una imagen .ppm el alto, ancho y el valor maximo rgb
 * @param arg Imagen de entrada
 * @param tamano String auxiliar
 * @param maximo Valor maximo rgb pasado por referencia
 * @return Retorna la posicion en el archivo donde comienza el mapa RGB
 ** */
int leerArchivo(char * arg, char tamano[20], int &maximo) {
	
	char cadena[300]; cadena[0] ='\0';
	bool leer = false;
	int salida = 0;
	
	ifstream entrada(arg);
	
	while(!entrada.eof()) {
				
		entrada.getline(cadena, 300);
		if(strcmp(cadena,"P3")) {
			leer = true;
		}
		if(leer) {
			if(cadena[0] == '#') {
				entrada.seekg(entrada.tellg());
				entrada.getline(cadena, 300);
				strcpy(tamano,cadena);
				
				entrada.seekg(entrada.tellg());
				entrada.getline(cadena, 300);
				maximo = atoi(cadena);
				break;			
			}
		}
	}
	salida = entrada.tellg();
	entrada.close();
	return salida;
}

/**
 * @brief Lee de una imagen .ppm el mapa rgb
 * @param pos posicion donde comienza el mapa rgb
 * @param arg Imagen de entrada
 * @param a ancho
 * @param b alto
 * @param m mapa rgb formato (pixel x, pixel y, rgb)
 * @return Retorna el mapa rgb de la imagen
 ** */
void leerMapa(int pos, char * arg, int a, int b, int ***m) {
	
	char cadena[300];
	cadena[0] = '\0';
	
	ifstream entrada(arg);
	entrada.seekg(pos);
	
	for(int i = 0; i < a; i++) {
		for(int j = 0; j < b; j++) {
			entrada.getline(cadena, 300);
			m[i][j][0] = atoi(cadena);
			
			entrada.seekg(entrada.tellg());
			entrada.getline(cadena, 300);
			m[i][j][1] = atoi(cadena);
			
			entrada.seekg(entrada.tellg());
			entrada.getline(cadena, 300);
			m[i][j][2] = atoi(cadena);
		}
	}
		
	entrada.close();
}
#endif
