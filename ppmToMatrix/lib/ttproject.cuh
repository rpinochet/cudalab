#ifndef TTPROJECT_CUH
#define TTPROJECT_CUH

/**
 * Clase Imagen
 * 
 * 
 * */

class Imagen {
	private:
		int f;
		int c;
		int color;
		int ***pixel;
	public:
		
		Imagen();
		Imagen(int filas, int ancho, int maximo);
		Imagen(Imagen const &);
		~Imagen();
		
		void getMapa(int ***m);
		void mostrarMapa();
	
};
#endif
