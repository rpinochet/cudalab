#include <iostream>
#include <cuda.h>
#include <fstream>
#include <string>

#include "lib/ttproject.cu"

using namespace std;

int main(int argc, char *argv[]) {
	char tamano[20];
	int maximo   = 0;
	int posicion = 0;
	int ***mapa;
	int f        = 0;
	int c        = 0;

	if( argc <= 1 ) {
		cout<<"no ingresaste archivos por linea de comandos"<<endl;
		return 0;
	}
	
	posicion=leerArchivo(argv[1], tamano, maximo);
	
	f = alto(tamano);
	c = ancho(tamano);
	
	mapa = new int**[f];
	for(int i = 0; i < f; i++) {
		mapa[i] = new int*[c];
		for(int j = 0; j < c; j++) {
			mapa[i][j]    = new int[3];
			mapa[i][j][0] = 0;
			mapa[i][j][1] = 0;
			mapa[i][j][2] = 0;
		}
	}
		
	leerMapa(posicion, argv[1], f, c, mapa);
	
	Imagen prueba(f, c, maximo);
	
	prueba.getMapa(mapa);
	prueba.mostrarMapa();
	
	return 0;
}

// imprime el mapa rgb
// for(int x=0;x<f;x++){
		// for(int y=0;y<c;y++){
			// cout<<mapa[x][y][0]<<","<<mapa[x][y][1]<<","<<mapa[x][y][2]<<"    ";
		// }
		// cout<<endl;
	// }
	
	//cout<<alto(tamano)<<" "<<ancho(tamano)<<" "<<maximo;
